# -*- encoding: utf-8 -*-

import argparse

import rospy

from convoy_common.msg import ManagementCommand, ControlCommand


def car_controller(car_id, mode, v_max, d_ref):
    mgmt_pub = rospy.Publisher('convoy_car_{}_mgmt'.format(car_id), ManagementCommand, queue_size=10)
    cntrl_pub = rospy.Publisher('convoy_car_{}_common_cntrl'.format(car_id), ControlCommand, queue_size=10)

    rospy.init_node('convoy_controller')
    rate = rospy.Rate(2)

    msg = ManagementCommand(mode=mode)
    rospy.loginfo(msg.mode)
    mgmt_pub.publish(msg)

    while not rospy.is_shutdown():
        cntrl_msg = ControlCommand(v_max=v_max, d_ref=d_ref)
        rospy.loginfo(cntrl_msg)
        cntrl_pub.publish(cntrl_msg)
        rate.sleep()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-c', '--car_id', help='Car id.', type=int)
    parser.add_argument('-m', '--mode', help='Mode', type=str, default='COMMON_CNTRL')
    parser.add_argument('-v', '--v_max', help='Maximal speed.', type=float, default=0.0)
    parser.add_argument('-d', '--d_ref', help='Maximal distance between cars.', type=float, default=0.5)
    args = parser.parse_args()
    try:
        car_controller(args.car_id, args.mode, args.v_max, args.d_ref)
    except rospy.ROSInterruptException:
        pass
